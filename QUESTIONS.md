Questions for Company







- What is the average size of project groups?
- Are practices like SCRUM utilized company-wide?
- How many meetings per week?
- How long do you spend coding per day?
- When the site goes down, do developers sacrifice night and weekend time to fix it?


- How much time is split between tech debt and feature development?

Questions for interviewer

- How are OKRs determined? Do product managers request updates from engineers/eng management before drafting?
- I am curious about company culture with regard to engineering. For instance, are there hackathons or other events tailored to eng growth? I am excited to see there is a Gusto Engineering blog detailing some successes and challenges (https://engineering.gusto.com)
-
- on the day-to-day, how would you estimate the dev time vs planning/team meeting ratio?
- are there opportunities for mentorship?
-
