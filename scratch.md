scratch.md

Product Engineer and Full Stack Software Developer.

I thoroughly enjoy working with clients to identify pain points and ways to improve their workflow while simultaneously building the solution to their hurdles.

While my interest lies in the advancement of web technologies and the open web, most of my professional experience has been backend-oriented at the API level





Spittin’ Corporate Game
===

I find myself most comfortable working with a small but motivated team of engineers and designers to build a platform for other engineers.

The majority of my recent professional experience has been with Ruby/Rails, but personal projects in Python or NodeJS. I am a San Francisco resident and US Citizen. I prefer to work remotely with the option to come into or travel to an office occasionally.


Who am I?
- product-focused engineer excited to work on hard problems
    - concurrency, distributed systems, API authorization/limiting/responsiveness
- excited to learn more about unfamiliar territories
    - AI, ML, LLMs,

My career journey
- Graduated from the RIT in 2013,
    - I majored in computer science with concentrations in programming language theory and graph theory
- Worked majorly on the backend in Java from 2013-2018
- Pivoted to Ruby, Rails, light FE web application work at Sonder
- At Percy, I was one of three full stack software engineers where we developed product features across the backend and frontend

What am I looking for?
- a lot of product surface area for a small set of engineers
- looking to build from the ground up as a bonus
- opportunities for strong product ownership and carrying out the software development process from specifications, architecting, implementation, deployment, and maintenance.
- small engineering team that works somewhat closely
    - we all are fairly familiar with each other’s projects/OKRs and those who are ahead of schedule can help out if anyone is behind

Behavioral Questions
- Tell us about a time when you advocated for someone else
- Tell us about a project you are most proud of completing
-







Companies

I feel my skillset and what I am looking for in my next role align with what Science is seeking in the job description:

- I have been developing web applications with Ruby + Rails for over 5 years now and absolutely love its expressiveness!
- I desire strong sense of ownership and independence over a large product surface area.
- I work best on a small, close-knit team that fosters a positive, healthy work environment.

Additionally, I very much distrust "the cloud"! It's what's got me into self-hosting, regular backups, and owning my own data ;-)




Templates
--------

Full Stack Development (Ruby on Rails, EmberJS, MySQL, Docker, Kubernetes) on percy.io.
Led product and implementation efforts for large scale multi-quarter long projects: Data Deletion Service, Percy Enterprise.
Worked with stakeholders to ensure delivery of the products occurred in a timely manner and met their needs.
Owned several key critical services: version control integrations, publicly-facing API, major Ruby and Rails version upgrades across all codebases.
Responsible for several frontend components and respective backend functionality: paywall modals, contact BrowserStack sales team modal and form, visual diff sensitivity slider, access token rotation.


Full Stack Development (Ruby on Rails, ReactJS, PostgreSQL) on the Demand team.
Owned development of the Listings Platform which allowed for internal stakeholders to perform bulk CRUD operations across all company listings.
Optimized booking experience on sonder.com via A/B experiments.
Reduced sonder.com unit search page load time by 60% with listing deduplication.
Designed and built listing auditor to report listing discrepancies across OTAs.








PicnicHealth
-----------

Hey!  Josh Lindsay here.  I am a Senior Full Stack engineer.  I primarily focus on developing web applications in Ruby on Rails, Python, and NodeJS.  I lean more toward the backend and API development, but oftentimes dabble in the frontend to finish a feature or component.

I am looking to work for a startup with large product surface area that values autonomy and product ownership.  I'd love to learn more about PicnicHealth and the engineering challenges you're facing!

Logistically, I am a San Francisco resident and US Citizen. I prefer to work remotely with the option to come into or travel to an office occasionally.


Rippling
-------

I am Josh Lindsay, senior full stack engineer with ~8 yoe, 5+ Ruby on Rails + web development experience. I am interested in learning more about Rippling and available opportunities, perhaps remote ones if available.


Full Stack Development (Ruby on Rails, GraphQL, ReactJS, MySQL) on the Core Payroll Services team.
Responsible for maintaining APIs for creating and managing organization payrolls.


UpCodes
-----------

I am moving from the SF Bay Area and am searching for a new remote opportunity with a small but motivated fully-remote team.  UpCodes caught my eye whilst perusing Hacker News because I wish work on building an easy-to-use, customer-centric product.  I'd love to learn more about the construction industry, issues it faces, how UpCodes helps solve the problem, and what future solutions need to be built!


LaunchDarkly
------------

Hey! I've been a fan of LaunchDarkly since having used it extensively when I used to work for Percy.io! I have recently been in communication with Julia Burton about a fullstack role. Hoping there is still an opening as I am most definitely interested.

I am a product engineer primarily focused on developing web applications. I lean more toward the backend and API development. The majority of my recent professional experience has been with Ruby/Rails, but personal projects in Python or NodeJS. I am a San Francisco resident and US Citizen. I prefer to work remotely with the option to come into or travel to an office occasionally.


jam.dev
---

Tell us more about you and your engineering experience

Senior web application developer and product engineer with 11 years of experience. Last 5 years of professional experience has been Ruby on Rails web application development. Personal projects in Python or NodeJS.


HubSpot
---



Call with Kaleigh of Hubspot

Location agnostic across the US


Senior 1 (5+) - 170-185, RSU 100-120 3yrs, 200-220
- features, interview new hires
Senior 2 (8+) - 190-220, RSU 180-220 3yrs, 300-320
- lead teams


Systems
Sample problems ... build youtube

initial design and how to explain design choices
trade offs of technologies
proactively as clarifying questions

building performant web application
excalidraw or lucidspark


ONE IS LARGER THAN THE OTHER
- scratch
- gather reuirements

Diagram design, no so much api design


Weave in customer centricity to systems design


Coding
classic techincal challenge
share screen with IDE
more algorithmic
test, debug, compile
talk out loud

sample input and output
edge cases




- What does Hubspot do?

All in one AI-powered customer relationship management (CRM) platform for scaling companies.
  - Marketing
        generate leads and automate marketing
        analytics
  - Sales
        build pipelines and close deals
        Deal management
  - Service
        scale support and drive retention
        AI chatbot
        Customer success workspace
  - Content
        AI-powered content creation
        helps marketers create and manage content
        Brand voice
  - Operations
        activate and manage your data
        Data sync
        Programmable automation
  - Commerce
        collect payments and automate billing
        Invoices & subscriptions
        Quotes


- Why Hubspot?

I'd like to work for an established company.
Over the past five years my career, I've taken several risks with small startups.
While the experience I gained in the time was substantial, the comanies themselves struggled with product market fit, acquihires, loss of funding or overspending.

I seek stability.  I feel HubSpot being the leading CRM platform will provide that.









- Technical take home test: Get data from api, solve algorithmic problem, send data to another api - HR Interview - Coding interview: Easy leetcode question - System design interview: Design netflix
https://glassdoor.com/Interview/HubSpot-Interview-RVW83164622.htm


Stage 1 (3hr coding assignment) - Pull from API, then sort through the data, and then POST back. Make sure you know how to GET and POST to an API before starting. Also, you can use any language you want.
Stage 2 (Recruiter call) - A super quick 15-minute call where you answer behavioral questions such as a time you failed when you had to give feedback to a co-worker, when you had a conflict with a co-worker, etc. Also, make sure you're able to explain what Hubspot is and why you want to work there.
Stage 3 (1hr System Design & 1hr Coding) - For system design make sure you practice and have a template for how to answer the question (Youtube videos). And for the coding make sure you know how to write test cases and can calculate the time complexity of your algorithm. The actual coding question is easy, so try to optimize and test your solution and talk through your thought process.

Q) A time when I had failed
Q) A time I had to explain something technical to someone
Q) A time I had made a mistake
Q) A time when I had received feedback
Q) A time I gave feedback
Q) What does Hubspot do?
Q) Why Hubspot?
https://glassdoor.com/Interview/HubSpot-Interview-RVW81417678.htm


1) Online assessment 2) 15-min call with HR with behavioural questions 3) two 1-hour back-to back interviews. Javascript and Algorithms. I was asked what I would prefer for an interview, JS or System design. 4) 10-min feedback call followed by an offer Overall, interviewers were very nice, I had a great pleasure to talk to them during the interview, felt more like pair programming. HR was also very considerate, always answering my letters within a day, setting up a preparation call before the tech interview. I could really feel that they want you to succeed. The timeline from the home assignment to the offer was more than a month.

Behavioural: - what do you know about Hubspot? - Tell me about a time when you gave feedback - when you received feedback that was unpleasant - when you had to explain something technical to another person - when you had deadlines and how you managed them - when you made a mistake 1st interview (45 mins JS + 15 mins behavioral) - closures, prototypes, rest, spread params 2d interview (45 mins for one algo task + 15 mins behavioral) - leetcode level easy - find most commonly occured substring of string k - make sure to test and optimise everything - also discussed my previous work experience, projects and courses I took.
https://glassdoor.com/Interview/HubSpot-Interview-RVW82151707.htm




Close call

Coding was really good. Joined late. Was able to understand and solve the problem optimally

No concern about coding ability

Weather service was a good interview
Asked clarifying questions
Accounted for NWS being flaky
Very positive

Netflix
Candidate did decent job hitting
Scale of userbase

Did not have hands on experience
Not familiar with nuance with larger systems
Could have concerns

Able to identify






Dosu.dev
-------

https://blog.langchain.dev/iterating-towards-llm-reliability-with-evaluation-driven-development/

SQL and Python

Evaluation driven development

How to keep response time low when using LLMs at scale?

Interesting set of problems such as when users share log files

Dosu is a Swiftie?!?! AWESOME

What problems are you trying to solve in the short term
- data collection and tightening the feedback loop










Archive
======

OLDER
--------------

- Ansa (jlindsay90@gmail.com) (https://www.ansa.dev)
    - early stage
    - who are current customers?
    - how is the company funded?
    - expected salary for Senior Fullstack SWE?
    - Is the position remote? Where is the office located? How many days per week expected in the office?


Meltano (jlindsay90@gmail.com)
    -
- United States Digital Service
    - I am a product-focused engineer excited to push the bounds of computing and the open web. I enjoy tackling a breadth of problem sets: concurrency, distributed systems, API responsiveness and scaling. I seek an opportunity that fosters strong product ownership while carrying out the entirety of the software development lifecycle.
    -
    - Bonus info: I used to live and work in Northern VA. My family is still out there, so I'd like to visit occasionally :-)
- Nooks (jlindsay90@gmail.com) (https://nooks.ai)
    - https://www.loom.com/share/b195dc61a3d24dffadf02a484b0ec54f
    - Hmmmm Typescript on FE and BE





Some old stuff from forever ago





6 September 2020
------

I am a software engineer who has the most experience doing Java backend development but is interested in switching over to frontend and full stack development. Most of the work I had done art Sonder wads on the feont wend with Ruby on Rails and React components. One in fact even had React, Redux, Sagas

Although I am interested in doing more development on the frontend,  I am most interested in product engineering and working with a client to develop a product.



I define myself as a Product Engineer and Full Stack Software Developer.

I thoroughly enjoy working with clients to identify pain points and ways to improve their workflow while simultaneously building the solution to their hurdles.

While my interest lies in the advancement of web technologies and the open web, most of my professional experience has been backend-oriented at the API level





AncestryDNA
------

Hi!  I'm Josh Lindsay and am a software engineer in San Francisco.  I have a wide range of experience from startups to large corporations.  I specialize in Java backend development, particularly at the API level.  I have a few Python side-projects that involve website crawling and data aggregation.

Admittedly, I became interested in DNA studies from the 23andMe and AncestryDNA Amazon Prime Day kits.  I find the analysis comprehensive and results absolutely fascinating!  I am curious what technical challenges are offered in the space and how my expertise can help tackle some of them.

————————————


Siri Team Recruiter
------

Another question: Are there other opportunities available within Siri?  I am particularly interested in Natural Language Understanding and Computational Linguistics roles.

Having studied linguistics academically and a number of languages (Slavic and Romance) both academically and leisurely, I have long held a fascination with the abstraction of language.

It is my understanding that Siri currently supports 21 languages and is now at a place where it could benefit from more widespread language support.  I am confident my skill set and experience can effectively contribute to that goal.

I recall having an engaging interview with one of the engineers on the NL team about universal language structure.

————————————

IFTTT Hiring Manager
-----

My 4+ years of professional software development experience lie primarily in Java backend services and API development.  My most recent role at LendUp was building a microservice comprised of multiple APIs to manage financial transaction history and statement generation and publishing.

I do have a few projects I am working on (particularly utilizing headless Firefox to more easily handle network requests and session management), but I’ve not open sourced them as they could possibly breach terms of service for the respective games 😇

I believe I can provide immense value to IFTTT being a user of the service myself and wanting to see deeper, more consistent integration with services.  I value the flexibility and power services provide through means of APIs.  IFTTT partnering with a number of companies to solve the problem of creating tentative workarounds to completing tasks It gives a sense of owning one’s data in this technological world plagued by walled gardens.


I apply for this Full Stack position because have always held a fascination for client-side work.  I have a rudimentary understanding from building simple, backend-heavy web applications at my time at HotPads and on my website (https://j000sh.com).

————————————

Hi! I'm Josh Lindsay and am a software engineer in San Francisco. I have a range of experience from startups to large corporations and even some independent contracting. I specialize
