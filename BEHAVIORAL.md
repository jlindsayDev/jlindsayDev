About Me
========

Projects

Azure DevOps Integration
- github, bitbucket, gitlab, slack
- When trying to give a feature demo to Microsoft, I allowed Perfect to be the enemy of good
    - there were a list of essential features
        - update commit and pull request statuses during and after a build
        -

Data Deletion Service

This was the most challenging project of my career that spanned almost an entire year while I worked at Percy.

Challenges:
- Hitting MySQL database read/write lock wait timeouts
- Determine recent-use of deduplicated objects (images, resources, assets). like a massive, ongoing LRU cache

Percy Enterprise

Challenges:
- Main team member leaving the company after we ironed out product specifications
