<!-- The (first) h1 will be used as the <title> of the HTML page -->
# Joshua Lindsay

<!-- The unordered list immediately after the h1 will be formatted on a single
line. It is intended to be used for contact details -->
- <josh@jlindsay.dev>
- https://jlindsay.dev
- California, USA

<!-- ## Summary -->
<!-- The paragraph after the h1 and ul and before the first h2 is optional. It
is intended to be used for a short summary. -->
Senior full-stack product engineer specializing in API development and building customer-facing tools.


## Skills

 - Languages: Ruby on Rails, Python, JavaScript/TypeScript (NodeJS, EmberJS, React)
 - Experience: Web applications, REST API development, Test-driven development


## Experience

<!-- You have to wrap the "left" and "right" half of these headings in spans by
hand -->
## Senior Full-Stack Engineer (Ruby on Rails)
### <span>Senior Software Engineer, Gusto</span> <span>Oct 2023 -- Feb 2024</span>
### <span>Senior Software Engineer, Percy @ BrowserStack</span> <span>Nov 2020 -- Apr 2023</span>

### Full-Stack Engineer (Ruby on Rails)
### <span>Software Engineer, Sonder</span> <span>Nov 2018 -- Apr 2020</span>

### Software Engineer (Java)
### <span>Software Engineer, LendUp</span> <span>Dec 2016 -- Sep 2017</span>
### <span>Software Engineer, Amazon Lab126</span> <span>Feb 2016 -- Jun 2016</span>
### <span>Software Engineer, HotPads @ Zillow</span> <span>Jan 2014 -- Jul 2015</span>
### <span>Software Engineer, Brand Networks</span> <span>Mar 2013 -- Oct 2013</span>


## Projects

### <span>Percy Enterprise</span> <span>Aug 2016</span>

### <span>Data Deletion Service</span> <span>Nov 2021 -- Jul 2022</span>

### <span>Azure DevOps Integration</span> <span>Jan 2021 -- Apr 2021</span>

-

### <span>Sonder Internal Listings Platform</span> <span>Apr 2020</span>
### <span>Sonder.com Listing Deduplication</span> <span>Aug 2016</span>
### <span>Listing Auditor</span> <span>Apr 2020</span>


## Education

### <span>Rochester Institute of Technology, B.S. Computer Science</span> <span>Aug 2008 -- May 2013</span>

- Minors: Mathematics, Russian Language
- Concentrations: Graph Theory, Programming Language Theory, Linguistics
