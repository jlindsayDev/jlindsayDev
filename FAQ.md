# Frequently Asked Questions (FAQ)


- A time when I had failed

  Sonder pricing changesets.  Despite lots of testing throughout the entire tech stack and allowing the pricing team to test it out on test data, there was still one lingering bug that had yet to be found.

  We were in a rush to ship before the OKR deadline.  I deployed code on a Friday evening at 5:30pm.

  Bear in mind this was fairly early in my career!

- A time I had made a mistake


- A time I had to explain something technical to someone

Funnily enough, I would do this often in my past relationship to overcome hurdles!
My partner was non-technical so explaining things to them required a bit more analysis on my part for what parts were important.

In doing so, it would help me rethink assumptions I was making or looking at the bigger picture


- A time when I had received difficult feedback

During one of my performance reviews, a close coworker rightly pointed out that sometimes when hitting a wall on a difficult task I tend to shell up and keep to myself when encountering anxiety or a difficult problem.

Rather than becoming defensive, I understood what she brought to my attention was for th


- A time I gave feedback

At my role at Percy, my boss was a newly-appointed engineering manager. I was her only direct report, yet she and I worked closely together on the product


- A time working with a difficult team member

Working directly with a cofounder.  He was very personable, but was known across the team to have a "my way or no way" attitude.

Since there were only three of us as dedicated backend/API engineers (including him), we spent a lot of time working together.
